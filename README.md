# React Native Builder using Archlinux as base image

Because i'm curious, losers, and dumb like a hell, please pardon me.

## Reason

### Why need image builder if you can build by yourself?
Build react native app somehow long and waste my time. I build this image to help me reduce my box's load when building and developing. However, the main reason why i create my base image is, because i'm paranoid about security and privacy (hence, the builder inside this image collecting analytics data), and to make me more confident about my builder environment.

### Then, your image got bigger?
Of course, it bigger. But i think it's kind of trades-off. You may consider to build it on gitlab too :).

### Who use it?

	* saya
	* aku
	* gw
	* me
	
### How to use it

You can use my image by pulling the image using this command on your shell `docker pull registry.gitlab.com/meyer-docker-dev/rn-arch-builder`. You maybe want to do it using sudo. 
Or, you can use it on Gitlab CI/CD (recommended). The template to using this image on gitlab CI/CD is like:

	image: registry.gitlab.com/meyer-docker-dev/rn-arch-builder:latest

	stages:
	  - test
	  - build
	  - deploy

	auditNPMPack:
	  stage: test
	  cache:
		  key:
		    files:
			  - package.json
	  paths:
		  - node_modules
      untracked: true
	  before_script:
		  - yarn
	  script:
		  - yarn audit
		  - react-native doctor

	buildAPK:
	  stage: build
	  cache:
	     key:
	       files:
		    - package.json
	paths:
      - node_modules
    untracked: true

	before_script:
		- yarn
		script:
		- yarn "android:clean"
		- yarn "android:release"
		- yarn "android:stop"
	artifacts:
		paths:
		  - android/app/build/outputs/apk/release/
	expire_in: '1 hrs'
	only:
	   - master 
  
### What package inside this image?

I try to minimalize the packages being installed. You can see the packages on `package.lst` file.
Of course you can build the image by yourself and put pacakages you want to install on `package.lst` file. You may want to change the build tools version or user run build context using env var. Below i try to explain the env you can overrides on building your image:

* ENV BUILD_TOOL_VERSION "28.0.3" => version of android build tools
* ENV PLATFORM_VERSION 28 => version of platform tools
* ENV SDK_PATCHER "patcher;v4" => version of patcher (optionals)
* ENV SDK_TOOLS_LINUX "sdk-tools-linux-4333796.zip" => filename for pulling the Android SDK upstream
* ENV ANDROID_HOME /opt/android/sdk => path to android sdk
* ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk => path to java jdk
* ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/build-tools/${BUILD_TOOL_VERSION}
* ENV USER_BUILD "android" => user that will build the app


### LICENSE

The license as described by packages respectively .
