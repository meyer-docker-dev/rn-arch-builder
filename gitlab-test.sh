#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
NC='\033[0m'


set -e


function installingAuditTool {
    echo -e "\n${GREEN}installing arch-audit and awk${NC}\n"
    
    pacman -S arch-audit awk --noconfirm --needed --noprogressbar 
}


function auditPackageInstalled {
    echo -e "\n${GREEN}run audit on linux package installed.${NC}\n"

    local RES

    RES=$(arch-audit -u)

    if [ "$RES" ]; then
	echo -e "\n${RED}${RES}${NC}\n"
	exit 1
    else
	echo -e "${PURPLE}No risk found on installed packages.${NC}\n"
    fi
}


function checkInstalledSDKPackage {
    echo -e "\n${GREEN}check installed android sdk package${PURPLE}\n"
    sdkmanager --list | sed -e '/Available Packages/q' | awk '{print $1}'
}


function releaseBuildTestRoot {
    echo -e "\n${GREEN}create react-native app for build test (root).${NC}\n"

    react-native init testApp 1> /dev/null && \
	mkdir -p testApp/android/app/src/main/assets && \
	cd testApp && \
	react-native bundle --platform android --dev false --entry-file index.js \
		     --bundle-output "${PWD}/android/app/src/main/assets/index.android.bundle" \
		     --assets-dest "${PWD}/android/app/src/main/res" && \
	cd android && ./gradlew clean assembleRelease -x bundleReleaseJsAndAssets
}


function releaseBuildTestNonRoot {
    echo -e "\n${GREEN}create react-native app for build test (non-root).${NC}\n"

    react-native init testApp 1> /dev/null && \
	mkdir -p testApp/android/app/src/main/assets && \
	cd testApp && \
	react-native bundle --platform android --dev false --entry-file index.js \
		     --bundle-output "${PWD}/android/app/src/main/assets/index.android.bundle" \
		     --assets-dest "${PWD}/android/app/src/main/res" && \
	cd android && ./gradlew clean assembleRelease -x bundleReleaseJsAndAssets
}


installingAuditTool && \
    auditPackageInstalled && \
    checkInstalledSDKPackage && \
    releaseBuildTestRoot &&\
    releaseBuildTestNonRoot
