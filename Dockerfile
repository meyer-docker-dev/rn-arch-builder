FROM archlinux:latest

ENV BUILD_TOOL_VERSION "28.0.3"
ENV PLATFORM_VERSION 28
ENV SDK_PATCHER "patcher;v4"
ENV SDK_TOOLS_LINUX "sdk-tools-linux-4333796.zip"
ENV ANDROID_HOME /opt/android/sdk
ENV JDK_VERSION "8"
ENV JAVA_HOME "/usr/lib/jvm/java-${JDK_VERSION}-openjdk"
ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/build-tools/${BUILD_TOOL_VERSION}
ENV USER_BUILD "android"
ENV REACT_NATIVE_CLI_VERSION "@2.0.1"
ENV GRADLE_HOME /home/${USER_BUILD}
ENV GRADLE_PROP_FILE ${GRADLE_HOME}/gradle.properties

WORKDIR /tmp

COPY package.lst patch.lst /tmp/

RUN echo -e "[multilib]\nInclude = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf &&\
    pacman -Syu --noconfirm &&\
    pacman -S "jdk${JDK_VERSION}-openjdk" $(cat /tmp/package.lst) $(cat patch.lst) --needed --noconfirm &&\
    yarn global add "react-native-cli${REACT_NATIVE_CLI_VERSION}" &&\
    curl -O https://dl.google.com/android/repository/${SDK_TOOLS_LINUX} &&\
    mkdir -p ${ANDROID_HOME} &&\
    7z x -o${ANDROID_HOME} ${SDK_TOOLS_LINUX}  &&\
    yes | sdkmanager "tools" "platform-tools" "build-tools;${BUILD_TOOL_VERSION}" "platforms;android-${PLATFORM_VERSION}" "${SDK_PATCHER}" 1> /dev/null &&\
    yes | sdkmanager --licenses 1> /dev/null &&\
    yes 'y'| pacman -Scc 1> /dev/null &&\
    rm /tmp/${SDK_TOOLS_LINUX} &&\
    useradd ${USER_BUILD} -m -s /bin/bash -p $(openssl rand -base64 32) &&\
    setfacl -Rm ${USER_BUILD}:rwx ${ANDROID_HOME} &&\
    mkdir -p ${GRADLE_HOME} &&\
    echo "org.gradle.java.home=/usr/lib/jvm/java-11-openjdk" >> ${GRADLE_PROP_FILE}


ENV PATH ${PATH}:$(yarn global bin):$PATH

LABEL maintainer="meyer.ananda@gmail.com" \
      version="1.0.0" \
      platform="android platform:${PLATFORM_VERSION}" \
      build_tool_version=${BUILD_TOOL_VERSION} \
      java_sdk="openjdk${JDK_VERSION}" \
      base="archlinux/base" \
      upstream="https://gitlab.com/meyer-docker-dev/rn-arch-builder"
