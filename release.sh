#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
NC='\033[0m'

BASEIMAGE=$1
USERNAME=$2

set -eo pipefail

if [ ! "$BASEIMAGE" ]; then
    echo "please supply base image name as arg 1"
    exit 1
fi

createDockerFileTempl() {
    cat > Dockerfile-release <<EOF
FROM $BASEIMAGE

RUN pacman -S sudo --noconfirm --needed &&\\
    echo -e '${1} ALL=(ALL) NOPASSWD: /usr/bin/pacman\\nDefaults timestamp_timeout=0' | tee /etc/sudoers.d/99-builder 1> /dev/null &&\\
    echo 'Set disable_coredump false' | tee /etc/sudo.conf 1> /dev/null &&\\
    echo "alias pacman='sudo pacman'" | tee /home/${1}/.bashrc 1> /dev/null &&\\
    chown ${1}:${1} /home/${1}/.bashrc

WORKDIR /home/${1}

USER $1

LABEL maintainer="$(docker inspect --format '{{.Config.Labels.maintainer}}' $BASEIMAGE)" \\
      version="$(docker inspect --format '{{.Config.Labels.version}}' $BASEIMAGE)" \\
      platform="$(docker inspect --format '{{.Config.Labels.platform}}' $BASEIMAGE)" \\
      build_tool_version="$(docker inspect --format '{{.Config.Labels.build_tool_version}}' $BASEIMAGE)" \\
      java_sdk="$(docker inspect --format '{{.Config.Labels.java_sdk}}' $BASEIMAGE)" \\
      base="$(docker inspect --format '{{.Config.Labels.base}}' $BASEIMAGE)" \\
      upstream="$(docker inspect --format '{{.Config.Labels.upstream}}' $BASEIMAGE)" \\
      user_builder="non-root"
EOF
}

if [ "$USERNAME" ]; then
    echo "create user ${USERNAME} context on ${BASEIMAGE}."
    createDockerFileTempl $USERNAME
else
    echo "username for user context on ${BASEIMAGE} not supplied. assume you want default user"
    echo "create user android context on ${BASEIMAGE}."
    createDockerFileTempl "android"
fi
